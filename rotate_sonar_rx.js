input.onButtonPressed(Button.A, function () {
    range = 50
    // 1 dot = 50cm range
    basic.showLeds(`
        . . . . #
        . . . . .
        . . . . .
        . . . . .
        . . . . .
        `)
    for (let index = 0; index < 3; index++) {
        radio.raiseEvent(
        4800,
        4801
        )
        control.waitMicros(10)
    }
    music.play(music.tonePlayable(220, music.beat(BeatFraction.Whole)), music.PlaybackMode.UntilDone)
})
input.onButtonPressed(Button.B, function () {
    range = 150
    // 3 dots = 150cm range
    basic.showLeds(`
        . . . . #
        . . . . #
        . . . . #
        . . . . .
        . . . . .
        `)
    for (let index = 0; index < 3; index++) {
        radio.raiseEvent(
        4800,
        4803
        )
        control.waitMicros(10)
    }
    music.play(music.tonePlayable(494, music.beat(BeatFraction.Whole)), music.PlaybackMode.UntilDone)
})
radio.onReceivedValue(function (name, value) {
    if (name == "heading") {
        heading = value
    } else if (name == "cm") {
        cm = value
        rxtmo = 2
        if (cm <= range) {
            serial.writeLine("" + convertToText(cm) + "C" + convertToText(heading) + "D")
        }
    }
})
input.onLogoEvent(TouchButtonEvent.Pressed, function () {
    range = 100
    // 2 dots = 100cm range
    basic.showLeds(`
        . . . . #
        . . . . #
        . . . . .
        . . . . .
        . . . . .
        `)
    for (let index = 0; index < 3; index++) {
        radio.raiseEvent(
        4800,
        4802
        )
        control.waitMicros(10)
    }
    music.play(music.tonePlayable(294, music.beat(BeatFraction.Whole)), music.PlaybackMode.UntilDone)
})
let rxtmo = 0
let cm = 0
let heading = 0
let range = 0
music.play(music.stringPlayable("- - - - - - - - ", 120), music.PlaybackMode.InBackground)
for (let index = 0; index < 4; index++) {
    basic.showIcon(IconNames.House)
    basic.pause(200)
    basic.showIcon(IconNames.StickFigure)
    basic.pause(200)
}
led.setBrightness(72)
range = 100
// 2 dots = 100cm range
basic.showLeds(`
    . . . . #
    . . . . #
    . . . . .
    . . . . .
    . . . . .
    `)
radio.setGroup(18)
loops.everyInterval(600, function () {
    if (rxtmo > 0) {
        rxtmo += -1
        led.toggle(2, 2)
    }
})
